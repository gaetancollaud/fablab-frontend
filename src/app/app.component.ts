import {Component, OnInit, ViewChild} from "@angular/core";
import {CurrentUserService} from "./shared/security/current-user.service";
import {MdSidenav} from "@angular/material";
import {RoleType} from "./shared/data/role";

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

	protected sidenavOpen: boolean;

	public Role: any = RoleType;

	@ViewChild('sidenav')
	private sidenav: MdSidenav;

	constructor(private currentUserService: CurrentUserService) {
		this.sidenavOpen = true;
	}

	ngOnInit(): void {
		this.currentUserService.updateCurrentUser();
	}

	protected toggleSideNav(): void {
		this.sidenavOpen = !this.sidenavOpen;
		if (this.sidenavOpen) {
			this.sidenav.open();
		} else {
			this.sidenav.close();
		}
	}
}
