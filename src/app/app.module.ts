import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";

import {AppComponent} from "./app.component";
import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "./modules/home/home";
import {SharedModule} from "./shared/shared.module";

import "hammerjs";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {AuthModule} from "./modules/auth/auth.module";
import {MaterialModule} from "./shared/material.module";
import {LayoutModule} from "./layout/layout.module";

const routes: Routes = [
	{path: 'home', component: HomeComponent},
	{path: 'auth/...', component: AuthModule},
	{path: 'admin', loadChildren: './modules/admin/admin.module#AdminModule'},
	// {path: 'machines', loadChildren: './modules/machine/machine.module#MachineModule'},
	{path: 'reservations', loadChildren: './modules/reservation/reservation.module#ReservationModule'},
	{path: '**', redirectTo: '/admin', pathMatch: 'full'},
	// {path: '**', redirectTo: '/auth/login', pathMatch: 'full'},
];

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		FormsModule,
		HttpModule,
		RouterModule.forRoot(routes,
			// {enableTracing: true} // <-- debugging purposes only
		),

		SharedModule.forRoot(),
		MaterialModule,
		LayoutModule,

		AuthModule,

	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
