import {NgModule} from "@angular/core";
import {MenuItemComponent} from "./menu-item";
import {MdIconModule, MdSidenavModule, MdTabsModule} from "@angular/material";
import {RouterModule} from "@angular/router";


@NgModule({
	declarations: [
		MenuItemComponent,
	],
	imports: [
		RouterModule,

		MdSidenavModule,
		MdTabsModule,
		MdIconModule
	],
	exports: [MenuItemComponent],
})
export class LayoutModule {
}
