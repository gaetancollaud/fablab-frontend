import {Component, Input} from "@angular/core";

@Component({
	selector: 'menu-item',
	templateUrl: './menu-item.html',
	styleUrls: ['./menu-item.css']
})
export class MenuItemComponent {

	@Input()
	public link: string;

	@Input()
	public icon: string;

	@Input()
	public label: string;

	public constructor() {

	}

}
