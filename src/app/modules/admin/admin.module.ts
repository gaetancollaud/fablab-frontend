import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../../shared/shared.module";
import {AdminComponent} from "./admin";
import {MachineModule} from "../machine/machine.module";
import {MachineListTabComponent} from "../machine/machine/machine-list-tab";
import {MachineTypeTabComponent} from "../machine/type/machine-type-tab";
import {LayoutModule} from "../../layout/layout.module";
import {MdSidenavModule, MdTabsModule} from "@angular/material";

const routes: Routes = [
	{
		path: '', component: AdminComponent, children: [
		{path: 'machines', component: MachineListTabComponent},
		{path: 'machines/:id', component: MachineListTabComponent},
		{path: 'machines-types', component: MachineTypeTabComponent},
		{path: '**', redirectTo: 'machines', pathMatch: 'full'}
	]
	}
];

@NgModule({
	declarations: [
		AdminComponent,
	],
	imports: [
		RouterModule.forChild(routes),
		CommonModule,
		SharedModule,

		MdSidenavModule,
		MdTabsModule,

		LayoutModule,

		MachineModule


	],
	exports: [RouterModule],
	providers: [
	]
})
export class AdminModule {
}
