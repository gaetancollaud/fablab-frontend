import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../../shared/shared.module";
import {LoginComponent} from "./login";
import {MaterialModule} from "../../shared/material.module";

const routes: Routes = [
	{path: 'auth/login', component: LoginComponent},
];

@NgModule({
	declarations: [LoginComponent],
	imports: [
		RouterModule.forChild(routes),
		CommonModule,
		SharedModule,
		MaterialModule

	],
	providers: [],
	exports: [RouterModule]
})
export class AuthModule {
}
