import {AfterViewInit, Component, ViewChild} from "@angular/core";
import {MdInputDirective} from "@angular/material";

@Component({
	templateUrl: './login.html',
	styleUrls: ['./login.css']
})
export class LoginComponent implements AfterViewInit {

	@ViewChild(MdInputDirective)
	private emailInput: MdInputDirective;

	public constructor() {

	}

	ngAfterViewInit() {
		setTimeout(() => {
			this.emailInput.focus();
		});
	}
}
