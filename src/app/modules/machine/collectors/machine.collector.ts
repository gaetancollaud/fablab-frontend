import {ListCollector} from "../../../shared/components/list/list-collector";
import {MachineEO} from "../../../shared/data/machine";

export class MachineCollector extends ListCollector<MachineEO> {

	public getItemId(item: MachineEO): any {
		return item && item.id;
	}
}
