import {MachineTypeEO} from "../../../shared/data/machine";
import {AbstractDatastore, DatastoreConfig} from "../../../shared/abstract/abstract.datastore";
import {Injectable} from "@angular/core";
import {LogService} from "../../../shared/log/log.service";
import {MachineTypeApiService} from "../../../shared/api/machine-type-api.service";
import {Router} from "@angular/router";

@Injectable()
export class MachineTypeDatastore extends AbstractDatastore<MachineTypeEO> {

	constructor(logService: LogService, apiService: MachineTypeApiService, router: Router) {
		super(logService, apiService, DatastoreConfig.fromRoute(router));
	}
}
