import {MachineEO} from "../../../shared/data/machine";
import {AbstractDatastore, DatastoreConfig} from "../../../shared/abstract/abstract.datastore";
import {Injectable} from "@angular/core";
import {LogService} from "../../../shared/log/log.service";
import {MachineApiService} from "../../../shared/api/machine-api.service";
import {Router} from "@angular/router";

@Injectable()
export class MachineDatastore extends AbstractDatastore<MachineEO> {

	constructor(logService: LogService, apiService: MachineApiService, router: Router) {
		super(logService, apiService, DatastoreConfig.fromRoute(router));
	}

}
