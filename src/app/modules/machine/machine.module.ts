import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../../shared/shared.module";
import {MdInputModule, MdSelectModule, MdSidenavModule, MdTabsModule} from "@angular/material";
import {MachineDatastore} from "./datastore/machine.datastore";
import {PanelModule} from "../../shared/components/panel/panel.module";
import {MachineTypeDatastore} from "./datastore/machine-type.datastore";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MachineEditComponent} from "./machine/edit/machine-edit";
import {MachineListComponent} from "./machine/list/machine-list";
import {MachineTypeListComponent} from "./type/list/machine-type-list";
import {MachineTypeEditComponent} from "./type/edit/machine-type-edit";
import {MachinePriceComponent} from "./type/price/machine-price";
import {MachineListTabComponent} from "./machine/machine-list-tab";
import {LayoutModule} from "../../layout/layout.module";
import {ListModule} from "../../shared/components/list/list.module";
import {MachineTypeTabComponent} from "./type/machine-type-tab";

// const routes: Routes = [
// 	{
// 		path: '', component: MachinesComponent, children: [
// 		{path: 'list', component: MachineListTabComponent},
// 		{path: 'type', component: MachineTypeTabComponent},
// 		{path: '**', redirectTo: 'list', pathMatch: 'full'}
// 	]
// 	}
// ];

@NgModule({
	declarations: [
		MachineListTabComponent,
		MachineTypeTabComponent,

		MachineEditComponent,
		MachineListComponent,

		MachineTypeListComponent,
		MachineTypeEditComponent,

		MachinePriceComponent
	],
	imports: [
		// RouterModule.forChild(routes),
		CommonModule,
		SharedModule,
		PanelModule,
		LayoutModule,
		ListModule,

		FormsModule,
		ReactiveFormsModule,

		MdInputModule,
		MdSelectModule,
		MdTabsModule,
		MdSidenavModule

	],
	exports: [RouterModule],
	providers: [
		MachineDatastore,
		MachineTypeDatastore
	]
})
export class MachineModule {
}
