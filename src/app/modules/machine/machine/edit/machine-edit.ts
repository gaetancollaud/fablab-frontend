import {Component, OnInit} from "@angular/core";
import {AbstractEditComponent} from "../../../../shared/abstract/abstract.edit";
import {MachineDatastore} from "../../datastore/machine.datastore";
import {MachineTypeDatastore} from "../../datastore/machine-type.datastore";
import {MachineEO, MachineTypeEO} from "../../../../shared/data/machine";

@Component({
	selector: 'machine-edit',
	templateUrl: './machine-edit.html',
	styleUrls: ['./machine-edit.css']
})
export class MachineEditComponent extends AbstractEditComponent<MachineEO> implements OnInit {

	private types: MachineTypeEO[];

	public constructor(machineDatastore: MachineDatastore, private machineTypeDatastore: MachineTypeDatastore) {
		super(machineDatastore);
	}

	ngOnInit(): void {
		super.ngOnInit();
		this.subHelper.addSubscription(this.machineTypeDatastore.items.subscribe((list: MachineTypeEO[]) => this.types = list));
	}
}
