import {Component} from "@angular/core";
import {MachineEO} from "../../../../shared/data/machine";
import {MachineDatastore} from "../../datastore/machine.datastore";
import {AbstractListComponent} from "../../../../shared/abstract/abstract.list";
import {ColumnDefinition} from "../../../../shared/components/list/datastore-list";
import {MachineCollector} from "../../collectors/machine.collector";

@Component({
	selector: 'machine-list',
	templateUrl: './machine-list.html',
	styleUrls: ['./machine-list.css']
})
export class MachineListComponent extends AbstractListComponent<MachineEO> {

	public columns: ColumnDefinition[] = [
		{name: 'NAME', prop: 'name'},
		{name: 'TYPE', prop: 'machineType.name'}
	];

	public constructor(public machineDatastore: MachineDatastore) {
		super(machineDatastore, new MachineCollector());

	}

}
