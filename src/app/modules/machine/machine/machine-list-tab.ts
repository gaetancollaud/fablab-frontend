import {Component, OnDestroy, OnInit} from "@angular/core";
import {MachineDatastore} from "../datastore/machine.datastore";
import {ActivatedRoute} from "@angular/router";

@Component({
	selector: 'machine-list-tab',
	templateUrl: './machine-list-tab.html',
	styleUrls: ['./machine-list-tab.css']
})
export class MachineListTabComponent implements OnInit, OnDestroy {
	public constructor(private machineDatastore: MachineDatastore, private route: ActivatedRoute) {

	}

	ngOnInit(): void {
		this.machineDatastore.start(this.route);
	}


	ngOnDestroy(): void {
		this.machineDatastore.stop();
	}
}
