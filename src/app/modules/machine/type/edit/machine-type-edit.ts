import {Component, OnInit} from "@angular/core";
import {AbstractEditComponent} from "../../../../shared/abstract/abstract.edit";
import {MachineTypeEO} from "../../../../shared/data/machine";
import {MachineTypeDatastore} from "../../datastore/machine-type.datastore";

@Component({
	selector: 'machine-type-edit',
	templateUrl: './machine-type-edit.html',
	styleUrls: ['./machine-type-edit.css']
})
export class MachineTypeEditComponent extends AbstractEditComponent<MachineTypeEO> implements OnInit {

	public constructor(machineTypeDatastore: MachineTypeDatastore) {
		super(machineTypeDatastore);
	}


}
