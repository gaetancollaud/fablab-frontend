import {Component} from "@angular/core";
import {MachineTypeEO} from "../../../../shared/data/machine";
import {MachineTypeDatastore} from "../../datastore/machine-type.datastore";
import {AbstractListComponent} from "../../../../shared/abstract/abstract.list";

@Component({
	selector: 'machine-type-list',
	templateUrl: './machine-type-list.html',
	styleUrls: ['./machine-type-list.css']
})
export class MachineTypeListComponent extends AbstractListComponent<MachineTypeEO> {

	public columns = [
		{name: 'NAME', prop: 'name'},
		{name: 'TYPE', prop: 'machineType.name'}
	];

	public constructor(machineTypeDatastore: MachineTypeDatastore) {
		super(machineTypeDatastore, null);
	}

}
