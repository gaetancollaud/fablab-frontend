import {Component, OnInit} from "@angular/core";
import {PriceMachineEO} from "../../../../shared/data/machine";
import {AbstractListComponent} from "../../../../shared/abstract/abstract.list";

@Component({
	selector: 'machine-price',
	templateUrl: './machine-price.html',
	styleUrls: ['./machine-price.css']
})
export class MachinePriceComponent extends AbstractListComponent<PriceMachineEO> implements OnInit {


	public constructor() {
		super(null, null);
	}

}
