import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {ReservationEO} from "../../shared/data/reservation";
import {ReservationApiService} from "../../shared/api/reservation-api.service";

@Injectable()
export class ReservationDatastore {

	private _list: BehaviorSubject<ReservationEO[]>;

	constructor(private reservationApiService: ReservationApiService) {
		this._list = new BehaviorSubject(null);

	}

	public updateList(): void {
		this.reservationApiService.getAll().subscribe((r: ReservationEO[]) => this._list.next(r));
	}

	public get list(): BehaviorSubject<ReservationEO[]> {
		return this._list;
	}
}
