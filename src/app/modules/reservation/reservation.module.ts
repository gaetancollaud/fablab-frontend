import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../../shared/shared.module";
import {ReservationsComponent} from "./reservations";
import {ReservationDatastore} from "./reservation.datastore";
import {MaterialModule} from "../../shared/material.module";

const routes: Routes = [
	{path: '', component: ReservationsComponent},
];

@NgModule({
	declarations: [ReservationsComponent],
	imports: [
		RouterModule.forChild(routes),
		CommonModule,
		SharedModule,
		MaterialModule,

	],
	providers: [
		ReservationDatastore
	],
	exports: [
		RouterModule,
		ReservationsComponent
	]

})
export class ReservationModule {
}
