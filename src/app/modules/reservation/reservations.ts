import {Component, OnInit} from "@angular/core";
import {ReservationDatastore} from "./reservation.datastore";

@Component({
	selector: 'reservations',
	templateUrl: './reservations.html',
	styleUrls: ['./reservations.css']
})
export class ReservationsComponent implements OnInit {

	public constructor(public datastore: ReservationDatastore) {

	}

	ngOnInit(): void {
		this.datastore.updateList();
	}
}
