import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {AbstractReadWriteApiService} from "../api/abstract-read-write-api.service";
import {Logger, LogService} from "../log/log.service";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {SubscriptionHelper} from "../utils/subscription-helper";
import 'rxjs/add/operator/filter';

export class AbstractDatastore<ENTITY> {

	protected dsLog: Logger;
	private _currentItem: BehaviorSubject<ENTITY>;
	private _items: BehaviorSubject<ENTITY[]>;
	private _loading: BehaviorSubject<boolean>;
	protected subHelper: SubscriptionHelper;
	protected route: ActivatedRoute;

	constructor(logService: LogService, protected apiService: AbstractReadWriteApiService<ENTITY>, protected config: DatastoreConfig = new DatastoreConfig()) {
		this.dsLog = logService.getLogger('AbstractDatastore');
		this._currentItem = new BehaviorSubject(null);
		this._items = new BehaviorSubject([]);
		this._loading = new BehaviorSubject(false);
		this.subHelper = new SubscriptionHelper();
	}

	public start(route: ActivatedRoute = null): void {
		this.route = route;
		if (this.config.idUrlParam) {
			this.listenForUrl();
		} else if (this.config.selectFirst) {
			this.selectFirstAfterRefresh();
		}
		this.refreshList();
	}

	public stop(): void {
		this.subHelper.unsubscribeAll();
	}

	private listenForUrl(): void {
		//TODO unsubscribe
		let machineIdObs: Observable<number> = this.route.paramMap
			.map((params: ParamMap) => {
				this.dsLog.debug(`params`, params);
				return params.get('id');
			})
			.map((str: string) => parseInt(str) || -1);
		this.subHelper.addSubscription(Observable.combineLatest(this.items, machineIdObs).subscribe((res: [ENTITY[], number]) => {
			let list: ENTITY[] = res[0];
			let id: number = res[1];
			this.dsLog.debug(`id:${id}, list:${list && list.length}`)
			if (list && list.length && id) {
				let toSelect = list.find((e: any) => e.id === id);
				// if (!toSelect && this.config.selectFirst) {
				// 	toSelect = list[0];
				// }
				this._currentItem.next(toSelect);
			}
		}));

		this.subHelper.addSubscription(this.currentItem
			.filter((item: ENTITY) => item != null)
			.subscribe((item: any) => {
				this.config.router.navigate(['admin', 'machines', item.id]);
			}));
	}

	private selectFirstAfterRefresh(): void {
		this.subHelper.addSubscription(this.items.subscribe((list: ENTITY[]) => {
			if (this.config.selectFirst && list.length > 0 && this._currentItem.getValue() == null) {
				this._currentItem.next(list[0]);
			}
		}));
	}

	public refreshList(): void {
		this.dsLog.debug(`Refreshing list`);
		this._loading.next(true);
		this.apiService.getAll().subscribe((list: ENTITY[]) => {
			this.dsLog.trace(`${list.length} element retrieved`);
			this._loading.next(false);
			this._items.next(list);
		});
	}

	public select(e: ENTITY) {
		this.dsLog.debug(`Select entity`, e);
		this._currentItem.next(e);
	}

	public save(e: ENTITY) {
		this.dsLog.debug(`Saving entity`, e);
		this.apiService.save(e).subscribe((saved: ENTITY) => {
			this.dsLog.trace(`Entity saved, refreshing list`, e);
			this._currentItem.next(saved);
			this.refreshList();
		});
	}

	public get currentItem(): BehaviorSubject<ENTITY> {
		return this._currentItem;
	}

	public get items(): BehaviorSubject<ENTITY[]> {
		return this._items;
	}

	public get loading(): BehaviorSubject<boolean> {
		return this._loading;
	}
}

export class DatastoreConfig {
	public idUrlParam: string;
	public selectFirst: boolean;
	public router: Router;

	public static fromRoute(router: Router, idUrlParam: string = 'id'): DatastoreConfig {
		return new DatastoreConfig(idUrlParam, true, router);
	}

	constructor(idUrlParam: string = 'id', selectFirst: boolean = true, router: Router = null) {
		this.idUrlParam = idUrlParam;
		this.selectFirst = selectFirst;
		this.router = router;
	}

}
