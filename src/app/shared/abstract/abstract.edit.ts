import {OnDestroy, OnInit} from "@angular/core";
import {SubscriptionHelper} from "../utils/subscription-helper";
import {AbstractDatastore} from "./abstract.datastore";

export class AbstractEditComponent<ENTITY> implements OnInit, OnDestroy {
	public item: ENTITY;
	protected subHelper: SubscriptionHelper;

	constructor(protected datastore: AbstractDatastore<ENTITY>) {
		this.subHelper = new SubscriptionHelper;
	}

	ngOnInit(): void {
		this.subHelper.addSubscription(this.datastore.currentItem.subscribe((item: ENTITY) => {
			this.item = item;
		}));
	}

	ngOnDestroy(): void {
		this.subHelper.unsubscribeAll();
	}
}
