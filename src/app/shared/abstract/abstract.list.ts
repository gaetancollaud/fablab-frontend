import {OnDestroy, OnInit} from "@angular/core";
import {SubscriptionHelper} from "../utils/subscription-helper";
import {AbstractDatastore} from "./abstract.datastore";
import {ListCollector} from "../components/list/list-collector";

export class AbstractListComponent<ENTITY> implements OnInit, OnDestroy {
	public items: ENTITY[];
	public currentItem: ENTITY;
	protected subHelper: SubscriptionHelper;
	public loading: boolean = false;

	constructor(protected datastore: AbstractDatastore<ENTITY>, protected collector: ListCollector<ENTITY>) {
		this.subHelper = new SubscriptionHelper;
	}

	ngOnInit(): void {
		this.subHelper.addSubscription(this.datastore.items.subscribe((items: ENTITY[]) => {
			this.items = items;
		}));
		this.subHelper.addSubscription(this.datastore.currentItem.subscribe((currentItem: ENTITY) => {
			this.currentItem = currentItem;
		}));
		this.subHelper.addSubscription(this.datastore.loading.subscribe((loading: boolean) => {
			this.loading = loading;
		}));
	}

	ngOnDestroy(): void {
		this.subHelper.unsubscribeAll();
	}

	public onSelect({selected}: { selected: ENTITY[] }): void {
		this.datastore.select(selected[0]);
	}

}

