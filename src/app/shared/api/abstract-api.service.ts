import {Headers, Http, RequestOptionsArgs, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Observer} from "rxjs/Rx";
import 'rxjs/add/operator/share'

export class AbstractApiService {

	constructor(protected http: Http) {
	}

	protected get(url: string, allowLoadingPanel: boolean = true): Observable<any> {
		return this.handlers(url, this.http.get(this.getUrl(url)), allowLoadingPanel)
			.map(res => this.getData(url, res));
	}

	protected post(url: string, data?: any, allowLoadingPanel: boolean = true): Observable<any> {
		return this.handlers(url, this.http.post(this.getUrl(url), data ? JSON.stringify(data) : '', this.getJsonRequestOptions()), allowLoadingPanel)
			.map(res => this.getData(url, res));
	}

	protected postFormData(url: string, data: any): Observable<any> {
		return this.handlers(url, this.http.post(this.getUrl(url), data))
			.map(res => this.getData(url, res));
	}

	protected put(url: string, data?: any): Observable<any> {
		return this.handlers(url, this.http.put(this.getUrl(url), data ? JSON.stringify(data) : '', this.getJsonRequestOptions()))
			.map(res => this.getData(url, res));
	}

	protected delete(url: string): Observable<any> {
		return this.handlers(url, this.http.delete(this.getUrl(url), this.getJsonRequestOptions()))
			.map(res => this.getData(url, res));
	}

	protected getUrl(url: string): string {
		return '/api/' + url;
	}

	protected getData(url: string, response: Response): any {
		try {
			let json: any = response.text().length === 0 ? null : response.json();
			return this.hasWrapper(url) ? json.data : json;
		} catch (e) {
			console.error("Cannot parse json for response ", response, e);
		}
		return null;
	}

	private getJsonRequestOptions(): RequestOptionsArgs {
		return {headers: this.getJsonHeader()};
	}

	private getJsonHeader(): Headers {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		return headers;
	}

	protected handlers(url: string, obs: Observable<Response>, allowLoadingPanel: boolean = true): Observable<Response> {
		return this.loadingPanelSubsriber(url, allowLoadingPanel, this.responseAnalyser(url, obs));
	}

	private responseAnalyser(url: string, obs: Observable<Response>): Observable<Response> {
		let ret: Observable<Response> = Observable.create((observer: Observer<Response>) => {
			obs.subscribe(res => {
				if (res.status !== 200) {
					// TODO interceptor
					// if (this.interceptor) {
					// 	this.interceptor.apiRequestError(url, res);
					// }
					observer.error(res);
				} else {
					try {
						if (this.hasWrapper(url)) {
							try {

								let jsonRes = res.json();
								// we can analyse the wrapper
								if (jsonRes.success === true) {
									observer.next(res);
								} else {
									observer.error(res);
								}
							} catch (e) {
								observer.error(res);
								console.error("Cannot parse response", res, e);
							}
						} else {
							// we assume there is no wrapper
							observer.next(res);
						}
					} catch (e) {
						observer.error(e);
					}
				}
			}, res => {
				//TODO
				// if (this.interceptor) {
				// 	this.interceptor.apiRequestError(url, res);
				// }
				observer.error(res);
			}, () => {
				observer.complete();
			});
		}).share();
		return ret;
	}

	protected loadingPanelSubsriber(url: string, allowLoadingPanel: boolean, obs: Observable<any>): Observable<any> {
		//TODO
		// if (this.interceptor && allowLoadingPanel) {
		// 	let loadingId: number = this.interceptor.apiRequestBegin(url);
		// 	let done = () => this.interceptor.apiRequestEnd(url, loadingId);
		// 	obs.share().subscribe(done, done);
		// }
		return obs;
	}

	private hasWrapper(url: string): boolean {
		let index = url.indexOf('v1/auth');
		return index === -1;
	}
}

