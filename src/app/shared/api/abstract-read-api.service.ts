import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {AbstractApiService} from "./abstract-api.service";

export class AbstractReadApiService<T> extends AbstractApiService {

	constructor(protected baseUrl: string, http: Http) {
		super(http);
	}

	public getAll(): Observable<T[]> {
		return this.get(this.baseUrl);
	}

	public getOne(id: number): Observable<T> {
		return this.get(`${this.baseUrl}/${id}`);
	}
}

