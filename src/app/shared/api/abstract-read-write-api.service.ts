import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {AbstractReadApiService} from "./abstract-read-api.service";

export class AbstractReadWriteApiService<T> extends AbstractReadApiService<T> {

	constructor(baseUrl: string, http: Http) {
		super(baseUrl, http);
	}

	public save(entity: T): Observable<T> {
		//TODO how to get id ?
		return this.put(`${this.baseUrl}/0`, entity);
	}

	public remove(id: number): Observable<T> {
		return this.delete(`${this.baseUrl}/${id}`);
	}
}

