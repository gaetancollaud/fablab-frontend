import {NgModule} from "@angular/core";
import {MachineApiService} from "./machine-api.service";
import {AuthApiService} from "./auth-api.service";
import {HttpModule} from "@angular/http";
import {ReservationApiService} from "./reservation-api.service";
import {MachineTypeApiService} from "./machine-type-api.service";

@NgModule({
	imports: [
		HttpModule
	],
	providers: [
		AuthApiService,
		MachineApiService,
		MachineTypeApiService,
		ReservationApiService,
	]
})
export class ApiModule {
}
