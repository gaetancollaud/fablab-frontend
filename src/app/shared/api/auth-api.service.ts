import {AbstractApiService} from "./abstract-api.service";
import {Http} from "@angular/http";
import {AuthCredential, ConnectedUser, LoginResult} from "../data/user";
import {Observable} from "rxjs/Observable";
import {Injectable} from "@angular/core";

@Injectable()
export class AuthApiService extends AbstractApiService {
	public constructor(http: Http) {
		super(http);
	}

	public login(credentials: AuthCredential): Observable<LoginResult> {
		return this.post('v1/auth/login', credentials);
	}

	public getConnectedUser(): Observable<ConnectedUser> {
		return this.get('v1/auth/current');
	}
}
