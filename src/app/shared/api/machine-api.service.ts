import {Http} from "@angular/http";
import {MachineEO} from "../data/machine";
import {Injectable} from "@angular/core";
import {AbstractReadWriteApiService} from "./abstract-read-write-api.service";

@Injectable()
export class MachineApiService extends AbstractReadWriteApiService<MachineEO> {
	constructor(http: Http) {
		super('v1/machine', http);
	}

}
