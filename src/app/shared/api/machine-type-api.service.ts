import {Http} from "@angular/http";
import {MachineTypeEO} from "../data/machine";
import {Injectable} from "@angular/core";
import {AbstractReadWriteApiService} from "./abstract-read-write-api.service";

@Injectable()
export class MachineTypeApiService extends AbstractReadWriteApiService<MachineTypeEO> {
	constructor(http: Http) {
		super('v1/machine-type', http);
	}

}
