import {Http} from "@angular/http";
import {PriceMachineEO} from "../data/machine";
import {Injectable} from "@angular/core";
import {AbstractReadWriteApiService} from "./abstract-read-write-api.service";

@Injectable()
export class PriceMachineApiService extends AbstractReadWriteApiService<PriceMachineEO> {
	constructor(http: Http) {
		super('v1/price-machine', http);
	}

}
