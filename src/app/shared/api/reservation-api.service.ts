import {Http} from "@angular/http";
import {AbstractReadApiService} from "./abstract-read-api.service";
import {Injectable} from "@angular/core";
import {ReservationEO} from "../data/reservation";

@Injectable()
export class ReservationApiService extends AbstractReadApiService<ReservationEO> {
	constructor(http: Http) {
		super('v1/reservation', http);
	}

}
