import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {ListCollector} from "./list-collector";
import {Logger, LogService} from "../../log/log.service";
import {AbstractDatastore} from "../../abstract/abstract.datastore";
import {SubscriptionHelper} from "../../utils/subscription-helper";
import {Observable} from "rxjs/Observable";
import "rxjs/add/observable/combineLatest"

@Component({
	selector: 'datastore-list',
	templateUrl: 'datastore-list.html',
	styleUrls: ['datastore-list.css'],
})
export class DatastoreListComponent<T> implements OnInit, OnDestroy {
	protected subHelper: SubscriptionHelper;

	@Input()
	protected collector: ListCollector<T>;

	@Input()
	protected datastore: AbstractDatastore<T>;

	@Input()
	public columns: ColumnDefinition[];

	private log: Logger;

	public items: T[];
	public loading: boolean;
	public tableSelected: T[];

	constructor(logService: LogService) {
		this.log = logService.getLogger('DatastoreListComponent');
		this.subHelper = new SubscriptionHelper();
	}

	ngOnInit(): void {
		this.subHelper.addSubscription(this.datastore.items.subscribe((items: T[]) => this.items = items || []));
		this.subHelper.addSubscription(this.datastore.loading.subscribe((loading: boolean) => this.loading = loading));
		this.subHelper.addSubscription(Observable.combineLatest(this.datastore.items, this.datastore.currentItem).subscribe((arr: [T[], T]) => {
			let items: T[] = arr[0];
			let current: T = arr[1];
			if (items && current) {
				if (this.collector) {
					this.tableSelected = [items.find((l: T) => this.collector.haveSameId(l, current))];
				} else {
					this.tableSelected = [items.find((l: T) => l === current)];
				}
			} else {
				this.tableSelected = [];
			}
		}));
	}


	ngOnDestroy(): void {
		this.subHelper.unsubscribeAll();
	}

	public onSelect(event: { selected: T[] }) {
		this.log.debug('Item selected', event.selected[0]);
		this.datastore.select(event.selected[0]);
	}
}

export class ColumnDefinition {
	public name: string;
	public prop: string;
}
