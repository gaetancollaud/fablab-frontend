export abstract class ListCollector<T> {

	public abstract getItemId(item: T): any;

	public haveSameId(left: T, right: T): boolean {
		return left && right && this.getItemId(left) === this.getItemId(right);
	}
}
