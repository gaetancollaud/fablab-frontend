import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {DatastoreListComponent} from "./datastore-list";

@NgModule({
	declarations: [
		DatastoreListComponent
	],
	exports: [
		DatastoreListComponent
	],
	imports: [
		CommonModule,

		NgxDatatableModule,
	],
	providers: []

})
export class ListModule {

}



