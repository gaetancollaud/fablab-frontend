import {Component, EventEmitter, Injectable, Input, Output} from "@angular/core";

@Injectable()
export class PanelActionService {
	private dropdownMenu: DropdownPanelActionComponent[] = [];

	public registerDropdown(dropdown: DropdownPanelActionComponent) {
		this.dropdownMenu.push(dropdown);
	}

	public closeAllDropDown(): void {
		this.dropdownMenu.forEach((d: DropdownPanelActionComponent) => d.open = false);
	}
}

@Component({
	selector: 'simple-panel-action',
	template: `
<a class="btn">
	<i class="material-icons"
		(click)="clicked()">{{icon}}</i>
</a>`,
	styleUrls: ['panel-actions.css'],
})
export class SimplePanelActionComponent {
	@Input()
	public icon: string;

	@Output()
	protected callback: EventEmitter<any> = new EventEmitter();

	constructor(private service: PanelActionService) {
	}

	public clicked(): void {
		this.service.closeAllDropDown();
		this.callback.emit();
	}

}

@Component({
	selector: 'dropdown-panel-action',
	template: `
<div class="btn-group" [class.open]="_open">
	<a class="btn dropdown-toggle" (click)="clicked()"
		(mouseleave)="startCloseTimeout()"
		(mouseenter)="stopCloseTimeout()">
		<i class="material-icons">{{icon}}</i>
	</a>
	<ul class="dropdown-menu" [style.left]="leftPosition"
		(mouseleave)="startCloseTimeout()"
		(mouseenter)="stopCloseTimeout()">
		<ng-content></ng-content>
	</ul>
</div>`,
	styleUrls: ['panel-actions.css'],
})
export class DropdownPanelActionComponent {

	@Input()
	public icon: string;
	@Input()
	public leftPosition: number = 0;

	public _open: boolean;
	private closeTimeout: number;

	constructor(private service: PanelActionService) {
		service.registerDropdown(this);
		this._open = false;
	}

	public clicked(): void {
		this.service.closeAllDropDown();
		this._open = !this._open;
	}

	public startCloseTimeout(): void {
		this.closeTimeout = setTimeout(() => {
			this._open = false;
		}, 1000);
	}

	public stopCloseTimeout(): void {
		if (this.closeTimeout) {
			clearTimeout(this.closeTimeout);
			this.closeTimeout = 0;
		}
	}

	public set open(value: boolean) {
		this._open = value;
	}
}

@Component({
	selector: 'sub-panel-action',
	template: `<a class="sub-menu" (click)="clicked()"><ng-content></ng-content></a>`,
	styleUrls: ['panel-actions.css'],
})
export class SubPanelActionComponent {

	private _callback: EventEmitter<any> = new EventEmitter();

	constructor(private service: PanelActionService) {

	}

	public clicked(): void {
		this.service.closeAllDropDown();
		this._callback.emit();
	}

	@Output()
	public get callback(): EventEmitter<any> {
		return this._callback;
	}
}
