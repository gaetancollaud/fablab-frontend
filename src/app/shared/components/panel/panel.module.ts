import {NgModule} from "@angular/core";
import {PanelComponent} from "./panel";
import {DropdownPanelActionComponent, PanelActionService, SimplePanelActionComponent, SubPanelActionComponent} from "./panel-actions";
import {CommonModule} from "@angular/common";

@NgModule({
	declarations: [
		PanelComponent,

		SimplePanelActionComponent,
		DropdownPanelActionComponent,
		SubPanelActionComponent
	],
	exports: [
		PanelComponent,

		SimplePanelActionComponent,
		DropdownPanelActionComponent,
		SubPanelActionComponent
	],
	imports: [
		CommonModule,
	],
	providers: [
		PanelActionService
	]

})
export class PanelModule {

}

