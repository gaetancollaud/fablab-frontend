import {Component, Input} from "@angular/core";

@Component({
	selector: 'panel',
	templateUrl: 'panel.html',
	styleUrls: ['panel.css'],
})
export class PanelComponent {

	@Input()
	public panelTitle: string;
	@Input()
	public flex: string;
	@Input()
	public contentClass: string;


}
