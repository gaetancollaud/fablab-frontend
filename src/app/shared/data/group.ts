import {RoleEO} from "./role";

export class GroupEO {
	public id: number;
	public technicalName: string;
	public name: string;
	public roles: RoleEO[];
}
