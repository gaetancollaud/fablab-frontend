export class MachineEO {
	public id: number;
	public name: string;
	public introduction: string;
	public description: string;
	public image_url: string;
	public machineType: MachineTypeEO;
}

export class MachineTypeEO {
	public id: number;
	public technicalname: string;
	public name: string;
}

export class PriceMachineEO {
	public machineTypeId: number;
	public membershipTypeId: number;
	public equation: string;
	public unit: PriceUnit;
}


export type PriceUnit = "HOUR" | "GRAMME";

export const PriceUnit = {
	HOUR: "HOUR" as PriceUnit,
	GRAMME: "GRAMME" as PriceUnit,
};
