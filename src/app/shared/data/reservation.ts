import {UserEO} from "./user";
import {MachineEO} from "./machine";


export class ReservationEO {

	public id: number;
	public dateStart: Date;
	public dateEnd: Date;
	public user: UserEO;
	public machine: MachineEO;

}
