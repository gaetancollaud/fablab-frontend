import {GroupEO} from "./group";

export class UserEO {
	public id: number;
	public firstname: string;
	public lastname: string;
	public email: string;
	public groups: GroupEO[];
}

export class ConnectedUser {
	public connected: boolean;
	public roles: string[];
	public user: UserEO;
}

export class AuthCredential {
	public login: string;
	public password: string;
}

export type LoginResult = "OK" | "UNKNOWN_USERNAME" | "WRONG_PASSWORD" | "INTERNAL_ERROR" | "ALREADY_CONNECTED" ;
export const LoginResult = {
	OK: "OK" as LoginResult,
	UNKNOWN_USERNAME: "UNKNOWN_USERNAME" as LoginResult,
	WRONG_PASSWORD: "WRONG_PASSWORD" as LoginResult,
	INTERNAL_ERROR: "INTERNAL_ERROR" as LoginResult,
	ALREADY_CONNECTED: "PANASONIC_WV_SERIES" as LoginResult,
};
