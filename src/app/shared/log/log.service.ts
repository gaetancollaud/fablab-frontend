import {Injectable} from "@angular/core";
import * as moment from 'moment';

/**
 * Loglevels
 */

export enum LogLevel {
	FATAL,
	ERROR,
	WARN,
	INFO,
	DEBUG,
	TRACE
}


/**
 * Log configuration (accessible via the global var logger)
 *
 */
export class LogConfiguration {

	private _specificLevel: Map<string, LogLevel>;
	private _wildcardLevel: Map<string, LogLevel>;
	private _globalLevel: LogLevel;
	private _localStoragePrefix = 'logger_';

	public constructor() {
		this._specificLevel = new Map();
		this._wildcardLevel = new Map();
		this._globalLevel = LogLevel.WARN;
		this.initFromLocalStorage();
	}

	public get specificLevel(): Map<string, LogLevel> {
		return this._specificLevel;
	}

	public get wildcardLevel(): Map<string, LogLevel> {
		return this._wildcardLevel;
	}

	public get globalLevel(): LogLevel {
		return this._globalLevel;
	}

	public setInitGlobalLevel(value: LogLevel) {
		this._globalLevel = this.readValue('global', value);
	}

	public setInitLogLevel(clazz: string, lvl: LogLevel) {
		this._specificLevel.set(clazz, this.readValue(clazz, lvl));
	}

	public setGlobalLevel(lvlStr: string) {
		let lvl = (<any>LogLevel)[lvlStr];
		this._globalLevel = lvl;
		this.writeValue('global', lvl);
	}

	public setLogLevel(clazz: string, lvlStr: string) {
		let lvl = (<any>LogLevel)[lvlStr];
		if (this.hasWildcard(clazz)) {
			this._wildcardLevel.set(clazz, lvl);
		} else {
			this._specificLevel.set(clazz, lvl);
		}
		this.writeValue(clazz, lvl);
	}

	public removeLogLevel(clazz: string) {
		if (this.hasWildcard(clazz)) {
			this._wildcardLevel.delete(clazz);
		} else {
			this._specificLevel.delete(clazz);
		}
		this.writeValue(clazz, undefined);
	}

	public clearAll() {
		this.getAllLocalStorageExistingKeys().forEach((key: string) => {
			window.localStorage.removeItem(key);
		});
		this._specificLevel = new Map();
		this._wildcardLevel = new Map();
		this._globalLevel = LogLevel.WARN;
	}

	public status() {
		/* tslint:disable */
		console.info(`Global ${LogLevel[this._globalLevel]}`);
		[this._wildcardLevel, this._specificLevel].forEach((l: Map<string, LogLevel>) => {
			l.forEach((v: LogLevel, k: string) => {
				console.info(`\t${k}\t${LogLevel[v]}`);
			});
		});
		/* tslint:enable */
	}

	private readValue(clazz: string, def: LogLevel): LogLevel {
		if (window.localStorage) {
			let lvlStr: string = window.localStorage.getItem(this.getLocalStorageKey(clazz));
			let lvl: LogLevel = (<any>LogLevel)[lvlStr];
			if (lvl) {
				return lvl;
			}
		}
		return def;
	}

	private writeValue(clazz: string, level: LogLevel) {
		if (window.localStorage) {
			if (level !== undefined) {
				window.localStorage.setItem(this.getLocalStorageKey(clazz), LogLevel[level]);
			} else {
				window.localStorage.removeItem(this.getLocalStorageKey(clazz));
			}
		}
	}

	private getLocalStorageKey(clazz: string) {
		return this._localStoragePrefix + clazz;
	}

	private initFromLocalStorage() {
		this.getAllLocalStorageExistingKeys().forEach((key: string) => {
			let clazz = key.substr(this._localStoragePrefix.length);
			let lvlStr = window.localStorage.getItem(this.getLocalStorageKey(clazz));
			let lvl: LogLevel = (<any>LogLevel)[lvlStr];
			if (clazz === 'global') {
				this._globalLevel = lvl;
			} else if (this.hasWildcard(key)) {
				this._wildcardLevel.set(clazz, lvl);
			} else {
				this._specificLevel.set(clazz, lvl);
			}
		});
	}

	private getAllLocalStorageExistingKeys(): string[] {
		let result: string[] = [];
		if (window.localStorage) {
			for (let i: number = 0; i < window.localStorage.length; i++) {
				let key = window.localStorage.key(i);
				if (key.startsWith(this._localStoragePrefix)) {
					result.push(key);
				}
			}
		}
		return result;
	}

	private hasWildcard(key: string): boolean {
		return key.indexOf('*') !== -1;
	}
}

//Global var to configure log
(<any>window).logger = new LogConfiguration();

declare var logger: LogConfiguration;

@Injectable()
export class LogService {

	private static instance: LogService;

	private clazzSize = 40;

	public static getInstance(): LogService {
		if (!LogService.instance) {
			LogService.instance = new LogService();
		}
		return LogService.instance;
	}

	public getLogger(clazz: string, inst: string = null): Logger {
		return new Logger(clazz, inst, this);
	}

	public log(lvl: LogLevel, clazz: string, msg: any[]) {
		if (this.hasToLog(lvl, clazz)) {

			let preMsg = moment().format('hh:mm:ss.SSS') + '\t' + LogLevel[lvl] + '\t' + this.getClazz(clazz);
			let fn = 'debug';
			switch (lvl) {
				case LogLevel.FATAL:
				case LogLevel.ERROR:
					fn = 'error';
					break;
				case LogLevel.WARN:
					fn = 'warn';
					break;
				case LogLevel.INFO:
					fn = 'info';
					break;
				default:
					fn = 'debug';
			}

			if (!(<any>console)[fn]) {
				fn = 'log';
			}

			if (msg.length === 1) {
				(<any>console)[fn](preMsg + msg[0]);
			} else if (msg.length === 2) {
				(<any>console)[fn](preMsg + msg[0], msg[1]);
			} else if (msg.length === 3) {
				(<any>console)[fn](preMsg + msg[0], msg[1], msg[2]);
			} else if (msg.length === 4) {
				(<any>console)[fn](preMsg + msg[0], msg[1], msg[2], msg[3]);
			} else {
				(<any>console)[fn](preMsg + msg[0], msg.splice(1, 1));
			}
		}
	}

	private hasToLog(lvl: LogLevel, clazz: string) {
		let specifiLevels: Map<string, LogLevel> = logger.specificLevel;
		return (!specifiLevels.has(clazz) && lvl <= logger.globalLevel) ||
			(specifiLevels.has(clazz) && lvl <= specifiLevels.get(clazz)) ||
			this.hasToLogWildcard(lvl, clazz);
	}

	private hasToLogWildcard(lvl: LogLevel, clazz: string) {
		let ret: boolean = false;
		logger.wildcardLevel.forEach((v: LogLevel, wild: string) => {
			if (this.wildcardMatch(clazz, wild) && lvl <= v) {
				ret = true;
			}
		});
		return ret;
	}

	private wildcardMatch(clazz: string, wildcard: string): boolean {
		let sub: string = wildcard.substr(0, wildcard.indexOf('*'));
		return clazz.startsWith(sub);
	}

	private getClazz(clazz: string): string {
		let ret = clazz;
		let space = this.clazzSize - clazz.length;
		for (let i = 0; i < space; i++) {
			ret += " ";
		}
		return ret;
	}
}

export class Logger {
	constructor(private clazz: string, inst: string, private service: LogService) {
		if (inst) {
			this.clazz += '(' + inst + ')';
		}
	}

	public error(...msg: any[]) {
		this.service.log(LogLevel.ERROR, this.clazz, msg);
	}

	public warn(...msg: any[]) {
		this.service.log(LogLevel.WARN, this.clazz, msg);
	}

	public info(...msg: any[]) {
		this.service.log(LogLevel.INFO, this.clazz, msg);
	}

	public debug(...msg: any[]) {
		this.service.log(LogLevel.DEBUG, this.clazz, msg);
	}

	public trace(...msg: any[]) {
		this.service.log(LogLevel.TRACE, this.clazz, msg);
	}
}
