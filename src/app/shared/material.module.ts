import {ModuleWithProviders, NgModule} from "@angular/core";
import {
	MdButtonModule,
	MdCardModule, MdIconModule,
	MdInputModule,
	MdListModule,
	MdSidenavModule,
	MdSlideToggleModule,
	MdTabsModule
} from "@angular/material";

@NgModule({
	imports: [

		MdTabsModule,
		MdButtonModule,
		MdInputModule,
		MdCardModule,
		MdSlideToggleModule,
		MdSidenavModule,
		MdListModule,
		MdIconModule,

	],
	declarations: [],
	providers: [],
	exports: [
		MdTabsModule,
		MdButtonModule,
		MdInputModule,
		MdCardModule,
		MdSlideToggleModule,
		MdSidenavModule,
		MdListModule,
		MdIconModule,
	]
})
export class MaterialModule {
	static forRoot(): ModuleWithProviders {
		return {
			ngModule: MaterialModule,
		};
	}
}
