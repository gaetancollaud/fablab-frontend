import {Injectable} from '@angular/core';
import {ConnectedUser} from "../data/user";
import {AuthApiService} from "../api/auth-api.service";
import {RxJsUtil} from "../utils/rxjs";
import {Logger, LogService} from "../log/log.service";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Observable} from "rxjs/Observable";

@Injectable()
export class CurrentUserService {

	private _currentUser: BehaviorSubject<ConnectedUser>;
	private log: Logger;

	constructor(logService: LogService, private authApiService: AuthApiService) {
		this._currentUser = new BehaviorSubject(null);
		this.log = logService.getLogger('CurrentUserService');
	}

	public updateCurrentUser(): Observable<ConnectedUser> {
		return RxJsUtil.makeHot(this.authApiService.getConnectedUser()
			.map((c: ConnectedUser) => {
				this.log.debug('current User changed for ', c);
				this._currentUser.next(c);
				return c;
			}));
	}

	public getCurrentUser(): BehaviorSubject<ConnectedUser> {
		return this._currentUser;
	}
}
