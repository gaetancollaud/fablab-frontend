import {Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef} from "@angular/core";
import {SecurityService} from "./security.service";
import {Logger, LogService} from "../log/log.service";
import {CurrentUserService} from "./current-user.service";
import {ISubscription} from "rxjs/Subscription";

@Directive({selector: '[hasRole]'})
export class HasRoleDirective implements OnInit, OnDestroy {
	private _prevCondition: boolean = null;
	private log: Logger;
	private _rolesStr: string;
	private currentUserSubscription: ISubscription;

	constructor(private _viewContainer: ViewContainerRef,
				private _templateRef: TemplateRef<Object>,
				private securityService: SecurityService,
				private currentUserService: CurrentUserService,
				logService: LogService) {
		this.log = logService.getLogger('HasRoleDirective');
	}

	ngOnInit(): void {
		this.currentUserSubscription = this.currentUserService.getCurrentUser().subscribe(() => this.update());
	}

	ngOnDestroy(): void {
		this.currentUserSubscription.unsubscribe();
	}

	@Input()
	public set hasRole(rolesStr: string) {
		this._rolesStr = rolesStr;
		this.update();
	}

	protected update(): void {
		if (this._rolesStr) {
			let roles: string[] = this._rolesStr.split(',');
			this.securityService.hasOneRole(...roles).subscribe((res: boolean) => {
				this.hasRoleResult(res);
				this.log.debug(`Has role ${this._rolesStr} : ${res}`);
			});
		} else {
			this.hasRoleResult(false);
			this.log.warn(`No role defined`);
		}
	}

	protected hasRoleResult(res: boolean) {
		if (res) {
			//user have role
			if (this.isBlank(this._prevCondition) || !this._prevCondition) {
				this._prevCondition = true;
				this._viewContainer.createEmbeddedView(this._templateRef);
			}
		} else {
			//user do not have role
			if (this.isBlank(this._prevCondition) || this._prevCondition) {
				this._prevCondition = false;
				this._viewContainer.clear();
			}
		}
	}

	private isBlank(obj: any): boolean {
		return obj === undefined || obj === null;
	}
}
