import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Injectable} from "@angular/core";
import {Logger, LogService} from "../log/log.service";
import {SecurityService} from "./security.service";
import {RoleType} from "app/shared/data/role";

/**
 * Guard to limit route depending on the role of the current user.
 *
 * Use data.roles of the route to define if list of roles. Example :
 *
 * const appRoutes: Routes = [
 * {
 * 		path: 'loading',
 * 		component: LoadingTest,
 * 		canActivate: [RoleGuard],
 * 		data:{
 * 			roles:[
 * 				RoleType.ROLE_BI_VIEWER,
 * 				RoleType.ROLE_LIVE_VIEWER
 * 			]
 * 		}
 * 	}
 *    ...
 *    ];
 *
 */
@Injectable()
export class RoleGuard implements CanActivate {
	private log: Logger;

	constructor(logService: LogService, readonly securityService: SecurityService) {
		this.log = logService.getLogger("RoleGuard");
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
		let roles = route.data["roles"] as Array<RoleType>;
		return this.securityService.hasOneRole(...roles)
			.map((result: boolean) => {
				if (result) {
					this.log.debug(`Route ${route.url} can be activated by roles ${roles}`);
				} else {
					this.log.warn(`Route ${route.url} cannot be activated by roles ${roles}`);
				}
				return result;
			})
			// as seen on https://github.com/angular/angular/issues/9613#issuecomment-228748731
			.take(1);
	}


}
