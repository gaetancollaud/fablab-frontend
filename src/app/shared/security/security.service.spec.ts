import {inject, TestBed} from "@angular/core/testing";

import {SecurityService} from "./security.service";
import {CurrentUserService} from "./current-user.service";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {AuthApiService} from "../api/auth-api.service";
import {ConnectedUser} from "../data/user";
import {RoleType} from "../data/role";
import {LogService} from "../log/log.service";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/observable/forkJoin'
import 'rxjs/add/operator/take'


describe('SecurityService', () => {
	let currentUser: BehaviorSubject<ConnectedUser>;

	let loginWithRole = (...roles: RoleType[]) => {
		let cu = new ConnectedUser();
		cu.connected = true;
		cu.roles = roles.map((r: RoleType) => 'ROLE_' + r);
		currentUser.next(cu);
	};
	let logout = () => currentUser.next(null);

	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				{
					provide: AuthApiService,
					useValue: null
				},
				CurrentUserService,
				SecurityService,
				LogService
			]
		});
	});

	it('should exists', inject([SecurityService, CurrentUserService], (securityService: SecurityService, currentUserService: CurrentUserService) => {
		currentUser = currentUserService.getCurrentUser();
		expect(securityService).toBeDefined('securityService');
		expect(securityService.isAuthenticated()).toBeFalsy('authenticated');
		loginWithRole();
		expect(securityService.isAuthenticated()).toBeTruthy('authenticated');
		logout();
	}));

	it('should not have roles', (done) => inject([SecurityService, CurrentUserService], (securityService: SecurityService, currentUserService: CurrentUserService) => {
		currentUser = currentUserService.getCurrentUser();

		securityService.hasOneRole(RoleType.USER_VIEW).subscribe((res: boolean) => {
			expect(res).toBeFalsy('anonymous_USER_VIEW');
			done();
		});
	})());
	it('should have roles', (done) => inject([SecurityService, CurrentUserService], (securityService: SecurityService, currentUserService: CurrentUserService) => {
		currentUser = currentUserService.getCurrentUser();

		loginWithRole(RoleType.USER_VIEW, RoleType.USER_MANAGE);
		Observable.forkJoin(
			securityService.hasOneRole(RoleType.USER_VIEW).take(1),
			securityService.hasOneRole(RoleType.USER_MANAGE).take(1),
			securityService.hasOneRole(RoleType.RESERVATION_VIEW).take(1)
		).subscribe((res: boolean[]) => {
			expect(res[0]).toBeTruthy('USER_VIEW');
			expect(res[1]).toBeTruthy('USER_MANAGE');
			expect(res[2]).toBeFalsy('RESERVATION_VIEW')
			logout();
			done();
		});
	})());
});
