import {Injectable} from "@angular/core";
import {CurrentUserService} from "./current-user.service";
import {ConnectedUser} from "../data/user";
import {RoleType} from "../data/role";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map'

@Injectable()
export class SecurityService {

	constructor(private currentUserService: CurrentUserService) {

	}

	public isAuthenticated(): boolean {
		let user: ConnectedUser = this.currentUserService.getCurrentUser().getValue();
		return user !== null && user.connected;
	}

	public hasOneRole(...roles: (RoleType | string)[]): Observable<boolean> {
		return this.currentUserService.getCurrentUser()
			.map((user: ConnectedUser) => {
				if (user && user.roles) {
					return roles
						.find((sr: RoleType | string) => user.roles
							.find((cr) => `ROLE_${sr}` === cr) !== undefined) !== undefined;
				} else {
					return false;
				}
			});
	}


}
