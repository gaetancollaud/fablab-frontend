import {ModuleWithProviders, NgModule} from "@angular/core";
import {CurrentUserService} from "./security/current-user.service";
import {ApiModule} from "./api/api.module";
import {HasRoleDirective} from "./security/has-role";
import {SecurityService} from "./security/security.service";
import {LogService} from "./log/log.service";

@NgModule({
	imports: [
		ApiModule,
	],
	declarations: [
		HasRoleDirective
	],
	providers: [],
	exports: [
		HasRoleDirective
	]
})
export class SharedModule {
	static forRoot(): ModuleWithProviders {
		return {
			ngModule: SharedModule,
			providers: [
				CurrentUserService,
				SecurityService,
				LogService
			]
		};
	}
}
