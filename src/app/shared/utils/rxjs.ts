import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";


export class RxJsUtil {

	public static  makeHot<T>(cold: Observable<T>): Observable<T> {
		const subject = new Subject();
		cold.subscribe(subject);
		return new Observable((observer) => subject.subscribe(observer));
	}
}
